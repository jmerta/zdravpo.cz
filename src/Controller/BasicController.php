<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\OpenHours;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BasicController extends AbstractController {

    /**
     * @return OpenHours
     */
    protected function getOpenHours(): OpenHours {
        $repo = $this->getDoctrine()->getRepository(OpenHours::class);
        return $repo->find(1);
    }
}
