<?php

namespace App\Controller;

use App\Entity\ProductCategory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends BasicController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        $repo = $this->getDoctrine()->getRepository(ProductCategory::class);

        $categories = $repo->findBy(['parent' => null], ['order' => 'ASC']);

        return $this->render('homepage/index.html.twig', [
            'categories' => $categories,
            'controller_name' => 'Úvod',
            'openHours' => $this->getOpenHours()
        ]);
    }
    /**
     * @Route("/kategorie/{categoryUrl}", name="homepageCategories")
     */
    public function homepageCategories($categoryUrl)
    {
        $repo = $this->getDoctrine()->getRepository(ProductCategory::class);
        $category = $repo->findOneBy(['url' => $categoryUrl]);

        $subCategories = $repo->findBy(['parent' => $category], ['order' => 'ASC']);

        return $this->render('homepage/category.html.twig', [
            'subCategories' => $subCategories,
            'category' => $category,
            'controller_name' => $category->getName(),
            'openHours' => $this->getOpenHours()
        ]);
    }
    /**
     * @Route("/cookies", name="cookies")
     */
    public function cookies()
    {

        return $this->render('homepage/cookiesInfo.html.twig', [
            'controller_name' => 'Cookies',
        ]);
    }
}
