<?php

namespace App\Controller;

use App\Entity\Company;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\Routing\Annotation\Route;

class StatisticsController extends EasyAdminController
{
    /**
     * @Route("/statistics", name="admin_stats")
     */
    public function index()
    {
        return $this->render('statistics/index.html.twig', []);
    }
}
