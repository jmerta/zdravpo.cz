<?php declare(strict_types=1);

namespace App\Form;

use App\Entity\Company;
use App\Entity\CompanyProduct;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanyProductFormType
 * @package App\Form
 */
class CompanyProductFormType extends AbstractType {

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(
            [
                'data_class' => CompanyProduct::class
            ]
        );
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'company',
                EntityType::class,
                [
                    'class' => Company::class,
                    'label' => 'Výrobce'
                ]
            )
            ->add(
                'link',
                TextType::class,
                [
                    'label' => 'Odkaz'
                ]
            );
    }


}
