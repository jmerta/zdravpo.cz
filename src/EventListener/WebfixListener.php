<?php

namespace App\EventListener;

use App\Entity\Company;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class WebfixListener
 * @package AppBundle\EventListener
 */
class WebfixListener {

    /**
     * @var TokenStorageInterface
     */
    private $storage;

    /**
     * WebfixListener constructor.
     * @param TokenStorageInterface $storage
     */
    public function __construct(TokenStorageInterface $storage) {

        $this->storage = $storage;
    }

    /**
     * @param LifecycleEventArgs $event
     * @throws Exception
     */
    public function preUpdate(LifecycleEventArgs $event) {
        $entity = $event->getEntity();
        if ($entity instanceof Company) {
            $entity->setUpdatedAt(new \DateTime('now'));
        }
    }

    /**
     * @param LifecycleEventArgs $event
     * @throws Exception
     */
    public function prePersist(LifecycleEventArgs $event) {
        $entity = $event->getEntity();
        if ($entity instanceof Company) {
            $entity->setUpdatedAt(new \DateTime('now'));
        }
    }

}
